# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=raft
pkgver=0.11.0
pkgrel=0
pkgdesc="C implementation of the Raft consensus protocol"
url="https://github.com/canonical/raft"
arch="all"
license="LGPL3"
makedepends="linux-headers libuv-dev autoconf automake libtool"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/canonical/raft/archive/v$pkgver.tar.gz
	unistd-include.patch"

prepare() {
	default_prepare
	autoreconf -i
}

build() {
	./configure \
		--prefix=/usr \
		--disable-lz4 \
		--enable-example=no
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

sha512sums="
808ea593e019ed1740ced2de4afd7f522056e08ccd739be77b069de80dd21e60dc9469395d6e18cb8f8056c12f4ef34859ca5892a3937280e708515a8bb3cebf  raft-0.11.0.tar.gz
d0f204a271ac40bf9b36027f2a0ee53195aad7366c0166a7f6268bac5bfb05546a5ef858f466c4bbc2a0c6a50dff8d9bc3314abd125f25d49ed5aab9641527b2  unistd-include.patch
"
